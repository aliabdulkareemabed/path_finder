#ifndef GRAPH_TYPES_H
#define GRAPH_TYPES_H
#include "util.h"
#include <stdbool.h>

#define NODE_SCALE_FACTOR 3
#define EDGE_SCALE_FACTOR 5

#define NODE_WIDTH  200 
#define NODE_HEIGHT 200 

#define DIGIT_WIDTH  100
#define DIGIT_HEIGHT 200

#define WEIGHT_TEXT_WIDTH  24
#define WEIGHT_TEXT_HEIGHT 40

typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t  i8;


typedef struct Edge Edge;
typedef struct Node Node;
typedef struct Graph Graph;
typedef struct Path Path;
typedef struct path_list path_list;

typedef struct dy_array edge_array;
typedef struct dy_array node_array;






static u64 e_id = 0XAA; //each edge will have an id, so similar edges will be recognised;
static u64 n_id = 0XBB;


enum NodeColorIndex
{
	N_Grey        = 0,
	N_Yellow      = 1,
	N_Orange      = 2,
	N_Blue        = 3,
	N_Green       = 4,
	N_Pink        = 5,
	N_Violate     = 6,
	N_Brown       = 7,
	N_Src_Color   = 8,
	N_Dest_Color  = 9,
};

#define N_Color_Count N_Src_Color

enum NodeNames
{

	Node_A = 0,
	Node_B, 
	Node_C, 
	Node_D, 
	Node_E,
	Node_F,
	Node_G,
	Node_H,
	Node_I,
	Node_J,
	Node_K,
	Node_L,
	Node_M,
	Node_N,
	Node_P
};

 struct Node
{
	edge_array edges;
	u32 int value;
	int x;
	int y;
	enum NodeColorIndex color;
	enum NodeColorIndex prevColor; //when switching back from shortest path to normal mode
	char name;	
	bool visited;
	u64 node_id;
};

struct Edge
{
	Node* first;
	Node* second;

	u32 edgeWeight;
	u64 e_id;
};

struct Graph
{
	node_array nodes;
	edge_array edges;
};

struct Path
{
	dy_array edges;
	u32 distance;
	bool shortest;
};

struct path_list
{
	dy_array paths;
};



Path Search(Node* start, Node* dest);
void PathFree(Path*);


#endif

