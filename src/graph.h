#ifndef GRAPH_H
#define GRAPH_H

// #include "util/util.h"
#include "util.h"
#include <limits.h>
#include <stdbool.h>

#include "graph_types.h"



static inline void PathCreate(Path* , size_t);


static inline void setTrue(Node* node)
{
	node->visited = true;
}

static inline void EdgesPrint(Node* node)
{
	size_t edgeCount = node->edges.count; // when we add to an edge the count 
	for(u32 i = 0; i < edgeCount ; ++i)
	{
		Edge* e = DYGETADDR(node->edges,Edge,i);
		Node* other = node == e->first?  e->second : e->first ;

		printf("%c\n", other->name);
	}
}


static inline void  EdgesConnect(Edge* e, Node* n1, Node* n2)
{
	e->first = n1;
	e->second = n2;
	e->e_id = ++e_id;
}

static Edge EdgesAdd(Node* n1, Node* n2, u32 edgeWeight)
{
	//very important to connect first otherwise they pointing to null
	Edge e;
	e.edgeWeight = edgeWeight;
	EdgesConnect(&e, n1,n2);
	DyArrayAddElem(&n1->edges, &e);
	DyArrayAddElem(&n2->edges, &e);
	return e;
	
}

static inline void NodesCopy(dy_array* nodesArrDest,dy_array* nodesArrSrc)
{
    DyArrayCopy(nodesArrDest,nodesArrSrc);

    for(size_t i = 0;i<nodesArrSrc->count;++i)
    {
        DyArrayCopy(&DYGETADDR(*nodesArrDest,Node,i)->edges,&DYGETADDR((*nodesArrSrc),Node,i)->edges);
    }

}
static inline void NodesFree(dy_array* nodes)
{
	if(nodes->state == dy_buf_init)
	{
		for(size_t i =0; i< nodes->count;++i)
    	{
        	Node* n = DYGETADDR(*nodes,Node,i);
        	DyArrayFree(&n->edges);
    	}
	    DyArrayFree(nodes);
	}
	else
	{
		printf("passed uninitalised buffer .... returning\n");
	}
    
}

static inline void EdgesCopy(dy_array* edgesDest, dy_array* edgesSrc)
{
    DyArrayCopy(edgesDest,edgesSrc);

    for(size_t i = 0 ; i<edgesSrc->count;++i)
    {
        *(DYGETADDR(*edgesDest,Edge,i)) = *(DYGETADDR(*edgesSrc,Edge,i));
    }   
}

static inline void EdgesFree(edge_array* edges)
{
	if(edges->state == dy_buf_init)
	{
        	DyArrayFree(edges);
	}

	else
	{
		printf("passed uninitalised buffer .... returning\n");
	}
	

}


static inline dy_array EdgesGet(Node* node)
{
	return node->edges;
}


static inline void EdgesCreate(edge_array* edges, size_t count)
{
	DyArrayInit(edges,count, sizeof (Edge));
}

static inline void EdgePush(edge_array* edges, Edge* e)
{
	DyArrayAddElem(edges,e);
}

static void PathCopy(Path* pDest, Path* pSrc)
{
	if(pDest->edges.state != dy_buf_init)
	{
		fprintf(stdout, "passed uninitalised path... allocating\n");
		PathCreate(pDest,2);
	}
	EdgesCopy(&pDest->edges, &pSrc->edges);
	pDest->distance = pSrc->distance;

}

void PathFree(Path* path)
{
	DyArrayFree(&path->edges);
}
static inline void NodesCreate(dy_array* nodes, size_t count)
{
		DyArrayInit(nodes,count, sizeof (Node));

}
static inline void NodesFill(dy_array* nodes)
{
	for(size_t i =0 ;i < nodes->capacity;++i)
	{
		int x =  5 + ((NODE_HEIGHT / NODE_SCALE_FACTOR) + 15) * (int)i;
		int y =  15 + ((NODE_HEIGHT / NODE_SCALE_FACTOR) + 60)* ((int)i/3);
		//dummy node to fill it up
		Node n;
		n.name =  (char)(i + 'A');
		n.visited = false;
		n.value = UINT_MAX; // Dijkstra algorithm assumes 
		n.color = i%(N_Color_Count - 1);
		n.prevColor = n.color;
		n.x = x;
		n.y = y;
		// add a node to dynamic array of nodes
		DyArrayAddElem(nodes,&n);

		/*Create a dynamic array of edges that holdes 2 edges at the start so each
		node can have 2 edges for the start*/
		DyArrayInit(&DYGETADDR(*nodes,Node,i)->edges ,2, sizeof (Edge));
	}
}
static inline void  PathCreate(Path* path, size_t count)
{

	DyArrayInit(&path->edges,count, sizeof(Edge));
	path->distance = 0;

}

static inline void PathAddEdge(Path* path, Edge* e)
{
	DyArrayAddElem(&path->edges, e);
	path->distance += e->edgeWeight;
}

static inline void PathPrint(Path* path)
{
	Edge* edge = DYGETADDR(path->edges,Edge,0);
	size_t count = path->edges.count;

	printf("Path travel: ");
	for(size_t i = 0; i<count; ++i)
	{
		char n1 = edge[i].first->name;
		char n2 = edge[i].second->name;
		printf(" %c --> %c ", n1,n2);
	}

	printf("\nAccumlated distance = %u \n",path->distance);
}



static inline void PathListCreate(path_list* p_list, size_t count)
{
	DyArrayInit(&p_list->paths,count,sizeof(Path));

}

static inline void PathListAdd(path_list* p_list, Path* path)
{

	Path temp;
	PathCreate(&temp,path->edges.capacity);

	PathCopy(&temp,path);
	DyArrayAddElem(&p_list->paths,&temp);

}

static inline void PathListFree(path_list* p_list)
{
	for(size_t i = 0, n  = p_list->paths.count; i<n; ++i)
	{
		PathFree(DYGETADDR(p_list->paths,Path,i));
	}

	DyArrayFree(&p_list->paths);
}

static inline void PathListPop(path_list* p_list, Path* path)
{
	if(p_list->paths.count <1)
	{

		fprintf(stdout,"the list is empty ... returning\n");
		return;

	}
	if(path == NULL)
	{
		fprintf(stderr, "passed null pointer .. returning\n");
		return;
	}
	if(path->edges.state == dy_buf_init)
	{
		#ifdef MEM_DEBUG
		fprintf(stdout,"possible of memory leak, passed inialised buffer\n");
		#endif
		PathFree(path);
	}


	DyArrayPop(&p_list->paths,path);

}


static inline void PathListPrint(path_list* p_list)
{
	for(size_t i =0; i < p_list->paths.count;++i)
	{
		PathPrint(DYGETADDR(p_list->paths,Path,i));
	}
}




static inline void PathListExpand(path_list* p_list, Path* poppedPath)
{	


	//---- pop and copy the top path in path list -----//

	

	size_t count = poppedPath->edges.count;
	if(count == 0)
	{
		fprintf(stderr,"PathListPop returned an empty path... existing\n");
		return;
	}

	//------ get the top edge of the top path -------//
	Edge* topEdge = DYGETADDR(poppedPath->edges,Edge, count-1);

	Node* n = topEdge->second;

	if(n->visited)
	{
		if(poppedPath->distance >= n->value) //the previous path was shorter, try the other node
		{
			n = topEdge->first;
			if(n->visited)
			{
				if(poppedPath->distance >= n->value)
				{
					goto GO_OUT;		
				}  
				else
				{
					n->value = poppedPath->distance;
				}
				
			}
			else
			{
				n->visited = true;
				n->value = poppedPath->distance;
			}
			
		}
		else //the previous path was longer, change it
		{
			n->value = poppedPath->distance;
		}
		
	}
	else
	{
		n->visited = true;
		n->value = poppedPath->distance;
	}

	edge_array edges = EdgesGet(n);

	for(size_t i = 0; i < edges.count; ++i)
	{
		Path tempPath;
		PathCreate(&tempPath,2);

		Edge* temp = DYGETADDR(edges,Edge,i);

		if(temp->e_id != topEdge->e_id)
		{
			PathCopy(&tempPath,poppedPath);
			PathAddEdge(&tempPath,temp);

			#ifdef MEM_DEBUG
			PathPrint(&tempPath);
			#endif

			PathListAdd(p_list,&tempPath);
		}
		PathFree(&tempPath);
		
	}

	GO_OUT:
	return;

}

static inline int compare(const void *path1, const void *path2)
{
	Path* p1 = (Path*) path1;
	Path* p2 = (Path*) path2;

	return p1->distance < p2->distance;
}

Path Search(Node* start, Node* dest)
{
	if(start == dest)
	{
		printf("start is the same as dest... returning\n");

		Path dumpPath;
		dumpPath.shortest = false;
		return dumpPath;
	}

	start->visited = true;
	edge_array edges = EdgesGet(start);

	path_list p_list; 
	PathListCreate(&p_list, 2);

	//adding the starting path edges (connects the starting node with any adjacent node)
	for(size_t i = 0; i < edges.count; ++i)
	{
		Path startPath;
		PathCreate(&startPath,2);
		PathAddEdge(&startPath,DYGETADDR(edges,Edge,i));
		PathListAdd(&p_list,&startPath);
		PathFree(&startPath);
	}
	qsort(p_list.paths.data, p_list.paths.count, sizeof(Path), compare);

	Path shortestPath;
	shortestPath.shortest = false;
	PathCreate(&shortestPath,2);

	while(p_list.paths.count > 0)
	{
		qsort(p_list.paths.data, p_list.paths.count, sizeof(Path), compare);	
		PathListPrint(&p_list);
		PathListPop(&p_list,&shortestPath);
		PathListExpand(&p_list,&shortestPath);

		if(dest->visited == true)
		{
			printf("Found the path! returning....\n");
			PathPrint(&shortestPath);
			PathListFree(&p_list);
			shortestPath.shortest = true;
			return shortestPath;
			
		}
		else
		{
			PathFree(&shortestPath);
		}


		
		

	}

	PathListFree(&p_list);
	printf("could not find it\n");
	return shortestPath;


}


  

static inline Graph GraphInit(void)
{

	Graph g;
	node_array nodes;
	edge_array edges;

	EdgesCreate(&edges, 3);

	NodesCreate(&nodes,6);
	NodesFill(&nodes);
	
	Edge temp;

	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_A), DYGETADDR(nodes,Node,Node_B), 4);
	EdgePush(&edges,&temp);
	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_A), DYGETADDR(nodes,Node,Node_C), 2);
	EdgePush(&edges,&temp);

	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_B), DYGETADDR(nodes,Node,Node_C), 1);
	EdgePush(&edges,&temp);
	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_B), DYGETADDR(nodes,Node,Node_D), 5);
	EdgePush(&edges,&temp);


	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_C), DYGETADDR(nodes,Node,Node_D), 8);
	EdgePush(&edges,&temp);
	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_C), DYGETADDR(nodes,Node,Node_E), 9);
	EdgePush(&edges,&temp);
	

	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_D), DYGETADDR(nodes,Node,Node_E), 2);		
	EdgePush(&edges,&temp);
	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_D), DYGETADDR(nodes,Node,Node_F), 6);		
	EdgePush(&edges,&temp);

	temp = EdgesAdd (DYGETADDR(nodes,Node,Node_E), DYGETADDR(nodes,Node,Node_F), 5);
	EdgePush(&edges,&temp);

	//----------- Code was movd into NodesGetStartNode()
	//----------- and NodesGetDestNode()
	// Node* startNode = DYGETADDR(nodes, Node, Node_A);
	// Node* destNode  = DYGETADDR(nodes, Node, Node_F);
	g.nodes = nodes;
	g.edges = edges;
	return g;

}


static inline void GraphFree(Graph* g)
{
	NodesFree(&g->nodes);
}

// static inline Node* NodesGetStartNode(void)
// {
// 	return DYGETADDR(nodes, Node, Node_A); //to be changed later 
// }

// static inline Node* NodesGetDestNode(void)
// {
// 	return DYGETADDR(nodes, Node, Node_F); //to be changed later to the UI
// }









#endif
