#ifndef KEYS_H
#define KEYS_H


typedef int Key;
#define Key_Space SDL_SCANCODE_SPACE
#define Key_S 	  SDL_SCANCODE_S
#define Key_D     SDL_SCANCODE_D
#define Key_P     SDL_SCANCODE_P
#define Key_R     SDL_SCANCODE_R

#endif

