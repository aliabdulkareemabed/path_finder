#ifndef RENDER_H
#define RENDER_H

#include "graph_types.h"




//------------ The API for the Application --------------//


void DrawNode(Node* node,int x, int y);
void DrawEdge(Edge* edge);
void DrawGraph(Graph* graph);
void RenderDraw(void);



#endif

