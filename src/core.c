#include "core.h"



static SDL_Window*   gWindow;
static SDL_Renderer* gRenderer;
static SDL_Texture*  gNodeTex;
static SDL_Texture*  gTextTex;
static bool Running = true;

static bool Init(void)
{
	if( SDL_Init(SDL_INIT_VIDEO ) != 0)
	{

		SDL_Log("could not init SDL error:%s\n",SDL_GetError());
		return false;
	}

	gWindow = SDL_CreateWindow("Graph Visualiser",
							   SDL_WINDOWPOS_UNDEFINED,
							   SDL_WINDOWPOS_UNDEFINED, 
							   SCREEN_WIDTH,
							   SCREEN_HEIGHT,
							   SDL_WINDOW_SHOWN);
	if(gWindow == NULL)
	{
		SDL_Log("could not create window error:%s\n",SDL_GetError());
		return false;		
	}

	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if(gRenderer == NULL)
	{
		SDL_Log("could not create renderer error:%s\n",SDL_GetError());
		return false;			
	}

	//--------------------------------------------------------//
	//---------------- Init SDL_Image ------------------------//

	int flags=IMG_INIT_JPG|IMG_INIT_PNG;
	int initted=IMG_Init(flags);
	if((initted&flags) != flags) 
	{
    	printf("IMG_Init: Failed to init required jpg and png support!\n");
    	printf("IMG_Init: %s\n", IMG_GetError());
    	// handle error
    	return false;
	}
	return true;

}

// static SDL_Surface* LoadImage(char const* path)
// {
// 	SDL_Surface* temp = IMG_Load(path);
// 	if(!temp)
// 	{
// 		 printf("IMG_Load error: %s\n", IMG_GetError());
//     	 return NULL;

// 	}
// 	return temp;

// }

static bool LoadAssets(void)
{

	SDL_Surface* NodeColor =  IMG_Load("assets/NodeColor.png");
	if(NodeColor == NULL)
	{
		return false;
	}

	gNodeTex =  SDL_CreateTextureFromSurface(gRenderer,NodeColor);
	SDL_FreeSurface(NodeColor);
	if(gNodeTex == NULL)
	{
		SDL_Log("failed to create texture error:%s\n",SDL_GetError());
		return false;	
	}
	SDL_Surface* Text = IMG_Load("assets/Text.png");
	gTextTex = SDL_CreateTextureFromSurface(gRenderer,Text);

	if(gTextTex == NULL)
	{
		SDL_Log("failed to create texture error:%s\n",SDL_GetError());
		return false;
	}
	return true;
}

static void Event(void)
{
	SDL_Event e;
	while(SDL_PollEvent(&e))
	{
		if(e.type == SDL_QUIT)
		{
			Running = false;
		}
	}

}


bool Setup(void)
{
	return Init() && LoadAssets();
}
SDL_Renderer* GetRenderer(void)
{
	return gRenderer;
}

SDL_Texture* GetNodeTex(void)
{
	return gNodeTex;
}

bool isRunning(void)
{
	Event();
	return Running;
}

void ClearBg()
{
	SDL_SetRenderDrawColor(gRenderer, 230, 200, 200, 255);
	SDL_RenderClear(gRenderer);
}
void Update(void)
{
	SDL_RenderPresent(gRenderer);
}

void GetMousePos(int* x, int* y)
{
    SDL_GetMouseState(x, y);
}


bool OnKeyPress(Key keyboardKey)
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	return state[keyboardKey];
}

SDL_Texture* GetTextTex(void)
{
	return gTextTex;
}
