#include "loadfromfile.h"
#include "graph_types.h"

/*
   A  B  C  D  E  F  G  H
A  -  2  1  -  2  -  1  -
B  2  -  -  1  -  2  -  5
C  1  -  -  -  1  -  -  4
D  -  1  -  -  2  5  7  9
E  2  -  1  2  -  1  2  3
F  -  2  -  5  1  -  -  -
G  1  -  -  7  2  -  -  8
H  -  5  4  9  3  -  8  -

however we can notice the symmetry and reduce it to the following:

A  2  1  -  2  -  1
B  -  1  -  2  -  5
C  -  1  -  -  4
D  2  5  7  9
E  1  2  3
F  
G  8


the ReadNodesFromFile assumes the latter representation of a graph within a file
*/

typedef struct pair_nodes
{
	int first_node_index;
	int second_node_index;
	u32 edgeWeight;
}pair_nodes;


static Graph LoadGraph(char* path)
{

	
	node_array nodes; 
	edge_array edges;
	NodesCreate(&nodes, 2);
	EdgesCreate(&edges,2);

	int i = 0;
    // int max_nodes = 26;
	char ch;
	char NodeName = 'A';
	int col = 1;
	int nodes_count = 0;
	while((ch = path[i++]))
	{
		if(NodeName > 'Z') break;

		switch(ch)
		{
			case '-': 
			{


				++col;
			} break;
			case ' ' :
			case ',' :
			case '\t':
			{

			} break;
			case '0':
			case '1': 
			case '2': 
			case '3': 
			case '4':
			case '5': 
			case '6':
			case '7': 
			case '8':
			case '9':
			{
				while(NodeName - 'A' + col + 1 > nodes_count)
				{
					printf("added Node %c\n", nodes_count +'A');
					//create a dump node and added it
					
					Node n;
					n.name =  (char)(nodes_count + 'A');
					n.visited = false;
					n.value = UINT_MAX; // Dijkstra algorithm assumes this
					n.color = N_Pink;
					n.prevColor = n.color;
					n.x = 25 + (40 * (nodes_count));
					n.y = 50 + (40 * nodes_count);
					EdgesCreate(&n.edges,2);

					NodesAdd(&nodes,&n);
					++nodes_count;
				}
				
				printf("Node: %c is connected to Node: %c with weight = %d\n", 
					NodeName, NodeName + (char) col, (u32) (ch - '0'));

				Node* n1 = DYGETADDR(nodes, Node, (int)(NodeName - 'A'));
				Node* n2 = DYGETADDR(nodes, Node, (int)(NodeName + (char) col - 'A'));

				Edge tempEdge = EdgesAdd(n1,n2, (u32) (ch - '0'));
				EdgePush(&edges,&tempEdge);

				++col;

			} break;

			case '\n':
			case '\r':
			{
				NodeName += 1;
				col = 1;
			} break;
		}
	}
	printf("Node count = %d\n",nodes_count);

	Graph g;
	g.nodes = nodes;
	g.edges = edges;
	return g;


}


static Graph ReadNodesFromFile(char const* path)
{


	FILE* file = fopen(path, "r");
	if(file == NULL)
	{
		printf("couldn't load file... returning\n");
		return (Graph){0};
	}


	// copying the whole file.
	fseek(file, 0L, SEEK_END);
	size_t fileSize = (size_t) ftell(file);
	rewind(file);

	char buffer[fileSize +1];
	size_t bytesRead  =  fread(buffer, sizeof(char), fileSize, file);
	buffer[bytesRead] = '\0';

	fclose(file);

	return LoadGraph(buffer);

	
}

