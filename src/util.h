#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdalign.h>
// #define MEM_DEBUG

#define DYGETADDR(Data, Type, Addr) \
            ((&((Type*)(Data).data)[(Addr)]))



typedef uint64_t u64;
typedef uint32_t u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef int64_t i64;
typedef int32_t i32;
typedef int16_t i16;
typedef int8_t  i8;

enum dy_buf_state
{
	dy_buf_init = 0X31,
	dy_buf_null = 0X13,
	dy_buf_undefined = 0X11
};


typedef struct dy_array
{
	void* data;
	size_t stride;
	size_t count;
	size_t capacity;
	enum dy_buf_state state;

} dy_array;

static inline void DyArrayInit(dy_array* dy_buf, size_t capacity, size_t elementSize)
{
	capacity = capacity < 2 ? 2 : capacity ; // 1.5 * capacity in DyArrayGrow will not scale if the capacity is 1
	dy_buf->data = (unsigned char*) malloc(capacity * elementSize);
	


	if(dy_buf->data == NULL)
	{
		fprintf(stderr, "failed to malloc memory\n");
		dy_buf->capacity = 0;
		dy_buf->stride 	 = 0;
		dy_buf->count 	 = 0;
		return ;
	}
	dy_buf->stride     = elementSize;
	dy_buf->capacity   = capacity;
	dy_buf->count 	   = 0;
	dy_buf->state      = dy_buf_init;  
}

static inline void DyArrayGrow(dy_array* dy_buf)
{
	void* temp = dy_buf->data;

	size_t new_capacity =  (size_t) (1.5 * (double)dy_buf->capacity);
	dy_buf->data = realloc(dy_buf->data, new_capacity * dy_buf->stride);

	#ifdef MEM_DEBUG
		printf("old block size %lu , new block size %lu \n",dy_buf->capacity, new_capacity);
	#endif
	if(dy_buf->data == NULL)
	{
		fprintf(stderr, "failed to realloc buffer\n");
		dy_buf->data = temp;
		return;
	} 
	dy_buf->capacity = new_capacity;
}

static inline void DyArrayAddElem(dy_array* dy_buf, void* val)
{
	if( dy_buf->count == dy_buf->capacity)
	{
		DyArrayGrow(dy_buf);
	}

	u8* whereToAdd = dy_buf->data;
	whereToAdd += (dy_buf->stride) * dy_buf->count;

	memcpy(whereToAdd, val, dy_buf->stride);
	dy_buf->count++;

}

static inline void DyArrayFree(dy_array* dy_buf)
{
	if(dy_buf->state == dy_buf_init)
	{
		free(dy_buf->data);
	    dy_buf->stride    = 0;
	    dy_buf->count     = 0;
	    dy_buf->capacity  = 0;
	    dy_buf->state = dy_buf_null;

	    return;
	}
	else
	{
		fprintf(stderr, "passed uninitalised buffer... returning\n");
	}
    
}

static inline void DyArrayCopy(dy_array* dest_buf, dy_array* src_buf)
{
    if(dest_buf == NULL || src_buf == NULL)
    {
        fprintf(stderr,"passed a null pointer to DyArrayCopy... existing\n");
        return;
    }

    if(dest_buf->state != dy_buf_init)
    {
    	fprintf(stdout,"passed inialised buffer... initalising it with capacity of source\n");
    	DyArrayInit(dest_buf,src_buf->capacity,src_buf->stride); // if the buffer has been initialised it is a mem leak.
    }

    
    while(dest_buf->capacity < src_buf->capacity)
    {
    	#ifdef MEM_DEBUG
    	fprintf(stderr,"dest buffer has less capacity is less than src capacity.... attemping to grow the buffer\n");
    	#endif
    	DyArrayGrow(dest_buf);
    }

    size_t bytesToCopy = src_buf->capacity* src_buf->stride;

    memcpy(dest_buf->data,src_buf->data,bytesToCopy);
    dest_buf->capacity = src_buf->capacity;

    dest_buf->count = src_buf->count;
    dest_buf->stride =src_buf->stride;

}

static inline void DyArrayPop(dy_array* dy_buf,void* poppedVal)
{
	//------ this assumes the memory has been allocated for the poopedVal-----//

	if(dy_buf->count ==0)
	{
		fprintf(stderr,"error, trying to pop an empty dy_array\n");
		return;
	}
	if(poppedVal == NULL)
	{
		fprintf(stderr,"error, trying passed a null pointer to fill \n");
		return;

	}
	dy_buf->count--;

	unsigned char* copyAt = (unsigned char*)dy_buf->data + (dy_buf->count) * dy_buf->stride;
	memcpy(poppedVal,copyAt, dy_buf->stride);

}




#endif

