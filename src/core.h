#ifndef CORE_H
#define CORE_H

#include <stdbool.h>

#include <SDL.h>
#include <SDL_image.h>
#include "keys.h"


#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480


typedef SDL_Texture*    Texture;
typedef SDL_Rect 		Rect;
typedef SDL_Renderer*   Renderer;




bool isRunning(void);
bool Setup(void);
bool OnKeyPress(Key keyboardKey);

void GetMousePos(int* x, int*y);

void ClearBg(void);
void Update (void);


SDL_Renderer* GetRenderer(void);
SDL_Texture* GetNodeTex(void);
SDL_Texture* GetTextTex(void);



#endif
