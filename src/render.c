#include "render.h"
#include "core.h"
#include "math.h"
#include "graph_types.h"




// static int IntAbs(int x)
// {
// 	return x < 0 ? -x : x;
// }

static bool HasPressed[128] = {0};

static Node* srcNode;
static Node* destNode;
Path shortestPath;

typedef struct Point
{
	int x;
	int y;

} Point;
typedef Point Vec2;


static void DragNode(Graph* graph);
static void ResetShortestPath(Graph* graph);
static void DrawShortestPath(Graph* graph, Node* src, Node* dest);

static int SquaredDistance(int x1, int y1, int x2, int y2)
{
	return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
}


struct 
{
	Uint8 r;
	Uint8 g;
	Uint8 b;
	Uint8 a;

} EdgeColor = {29,30,45,255};

void DrawNode(Node* n, int x, int y)
{

	static const Rect nodeRect = {0,0, NODE_WIDTH,NODE_HEIGHT};

	Rect destRect   = {x, y, NODE_HEIGHT / NODE_SCALE_FACTOR, NODE_HEIGHT / NODE_SCALE_FACTOR};
	Rect srcRect    = nodeRect;

	Texture nodeTex = GetNodeTex();
	Renderer renderer = GetRenderer();


	int xoffset = NODE_WIDTH * (n->color % 5); 	// the value 5 is how many Nodes in a texture's width
	int yoffset = NODE_HEIGHT * (n->color / 5);
	srcRect.x += xoffset;
	srcRect.y += yoffset;

	SDL_RenderCopy(renderer,
                   nodeTex,
                   &srcRect,
                   &destRect);
	
}

static void SelectSource(Graph* graph)
{

	int xMouse, yMouse;
	GetMousePos( &xMouse,&yMouse);

	int radius = (NODE_WIDTH / NODE_SCALE_FACTOR) / 2 ; 
	int offset = radius; // offset from the top right corner of the texture to the center of the texture

	Node* selectedNode = NULL;

	size_t const nodeCount = graph->nodes.count;
	for(size_t i = 0; i < nodeCount; ++i)
	{
			Node* n = DYGETADDR(graph->nodes,Node,i);

			if( SquaredDistance(xMouse,yMouse, n->x + offset, n->y + offset) <= radius * radius)
			{
				selectedNode = n;
				break;
			}	
	}


//------ put the previous source to its original value -----//
	if(selectedNode)
	{
		if(srcNode)
		{
			srcNode->color = srcNode->prevColor;	
		}
		
		srcNode = selectedNode;
		srcNode->color = N_Src_Color;
	}
	else if(srcNode)
	{
		srcNode->color = srcNode->prevColor;
	}

}

static void SelectDest(Graph* graph)
{
	int xMouse, yMouse;
	GetMousePos( &xMouse,&yMouse);

	int radius = (NODE_WIDTH / NODE_SCALE_FACTOR) / 2 ; 
	int offset = radius; // offset from the top right corner of the texture to the center of the texture

	Node* selectedNode = NULL;

	size_t const nodeCount = graph->nodes.count;
	for(size_t i = 0; i < nodeCount; ++i)
	{
			Node* n = DYGETADDR(graph->nodes,Node,i);

			if( SquaredDistance(xMouse,yMouse, n->x + offset, n->y + offset) <= radius * radius)
			{
				selectedNode = n;
				break;
			}	
	}
	
	if(selectedNode)
	{
		if(destNode)
		{
			destNode->color = destNode->prevColor;	
		}
		
		destNode = selectedNode;
		destNode->color = N_Dest_Color;
	}
	else if(destNode)
	{
		destNode->color = destNode->prevColor;
	}

}

static bool OnKeyDown(Key keyboardKey)
{
	bool isPressed = OnKeyPress(keyboardKey);
	bool prevPressed = !HasPressed[keyboardKey];

	HasPressed[keyboardKey] = isPressed;
	return prevPressed && isPressed;

}




void DrawGraph(Graph* graph)
{
	while(isRunning())
	{
		ClearBg();

		

		for(size_t i =0; i<graph->edges.count;++i)
		{
			Edge* e = DYGETADDR(graph->edges,Edge,i);
			DrawEdge(e);
		}

		for(size_t i =0; i< graph->nodes.count;++i)
		{	
			
			Node* n = DYGETADDR(graph->nodes,Node,i); 
			DrawNode(n, n->x ,n->y);
		}
		
		if(OnKeyPress(Key_Space)) 
		{
			DragNode(graph);	
		}
		else if(OnKeyDown(Key_S))
		{
			SelectSource(graph);
		}
		else if(OnKeyDown(Key_D))
		{
			SelectDest(graph);
		}
		else if(OnKeyDown(Key_P))
		{
			DrawShortestPath(graph,srcNode,destNode);
		}
		else if(OnKeyDown(Key_R))
		{
			ResetShortestPath(graph);
		}

		Update();	
	}
	
}













/* Get the mouse position, fill an array of graph node positions 
** Select the node closest to the mouse
** if the selection button (space) is pressed, move the the node to the mouse position
** if the node has been moved and collided with with another node, move it back.
*/
static void DragNode(Graph* graph)
{

	int xMouse, yMouse;
	GetMousePos( &xMouse,&yMouse);

	int radius = (NODE_WIDTH / NODE_SCALE_FACTOR) / 2 ; 
	int offset = radius; // offset from the top right corner of the texture to the center of the texture

	Node* selectedNode = NULL;

	size_t const nodeCount = graph->nodes.count;
	Point graphPositions[nodeCount];


	bool assigned = false;
	for(size_t i = 0; i < nodeCount; ++i)
	{
		Node* n = DYGETADDR(graph->nodes,Node,i);

		graphPositions[i].x  = n->x;
		graphPositions[i].y  = n->y;

		if(!assigned)
		{
			if( SquaredDistance(xMouse,yMouse, n->x + offset, n->y + offset) <= radius * radius)
			{
				selectedNode = n;
				assigned = true;
			}	
		}
	}

	if(selectedNode != NULL)
	{
		//------- making sure we don't collide with other nodes --------//
		int newX = xMouse - offset;  
		int newY = yMouse - offset;  

		bool collide = false;

		size_t i;
		for(i = 0; i < nodeCount; ++i)
		{
			char name = DYGETADDR(graph->nodes,Node,i)->name;
			if(selectedNode->name != name) // we are not checking the node with itself
			{		
				if(SquaredDistance(newX,newY, graphPositions[i].x ,graphPositions[i].y) <= (4 * radius * radius) )
				{
					//move the Node a bit backwards

					collide = true;
					int xOutVec = selectedNode->x - graphPositions[i].x;
					int yOutVec = selectedNode->y - graphPositions[i].y;
 
					// normalize the vector
					double len = sqrt(xOutVec * xOutVec + yOutVec * yOutVec);
					double normalizedX = xOutVec / len;
					double normalizedY = yOutVec / len;

					selectedNode->x  += (int)(2 *normalizedX);
					selectedNode->y  += (int)(2 *normalizedY);

					break;
				}
			}
			
		}
		if(!collide)
		{
			selectedNode->x = newX;
			selectedNode->y = newY;

			graphPositions[i].x = newX;
			graphPositions[i].y = newY;
		}
	}


}

static void ResetShortestPath(Graph* graph)
{

		for(size_t j = 0; j < graph->nodes.count; ++j)
		{
			Node* n = DYGETADDR(graph->nodes,Node,j);
			n->color = n->prevColor;
			n->visited = false;
		}
		srcNode = NULL;
		destNode = NULL;
}

static void DrawShortestPath(Graph* graph, Node* s, Node* d)
{
	if(s == NULL || d == NULL)
	{
		return;
	}

	shortestPath = Search(s,d);

	if(shortestPath.shortest)
	{
		for(size_t i =0; i < shortestPath.edges.count; ++i)
		{
			Edge* pEdge = DYGETADDR(shortestPath.edges,Edge,i);

			for(size_t j = 0; j < graph->edges.count; ++j)
			{
				Edge* gEdge = DYGETADDR(graph->edges,Edge,j);

				if(gEdge->e_id == pEdge->e_id)

				{
					
						gEdge->first->color = N_Brown;
						gEdge->second->color = N_Brown;

				}
			}
		}
		PathFree(&shortestPath);

	}
}



void DrawEdge(Edge* e)
{
	SDL_SetRenderDrawColor(GetRenderer(),EdgeColor.r,EdgeColor.g,EdgeColor.b,EdgeColor.a);
	Node* first  = e->first;
	Node* second = e->second;

	
	//---------------- Drawing the weight---------------//

	static const Rect textRect = {0,0, DIGIT_WIDTH,DIGIT_HEIGHT};

	Vec2 v = {first->x - second->x , first->y - second->y};

	//--------- find the midpoint ---------//
	Vec2 center = {(int)(v.x / 2) , (int)(v.y / 2)};

	//----- add the padding + TEXTURE offset to the from the edge of the texture to the middle


	center.x += (DIGIT_WIDTH  / EDGE_SCALE_FACTOR);
	// center.y -= (DIGIT_HEIGHT / EDGE_SCALE_FACTOR) / 2;


	Rect destRect   = {second->x + center.x, second->y + center.y, DIGIT_WIDTH / EDGE_SCALE_FACTOR, DIGIT_HEIGHT / EDGE_SCALE_FACTOR};
	Rect srcRect    = textRect;
	srcRect.x += e->edgeWeight * DIGIT_WIDTH;

	Texture textTex = GetTextTex();
	Renderer renderer = GetRenderer();

	



	// ---------------- position the edge at the center of each node ----------//
	int xoffset = (NODE_WIDTH / NODE_SCALE_FACTOR) / 2;
	int yoffset = (NODE_HEIGHT / NODE_SCALE_FACTOR) / 2;


	SDL_RenderDrawLine(renderer,
                       first->x  + xoffset,
                       first->y  + yoffset,
                       second->x + xoffset,
                       second->y + yoffset);
	// Rect destRect = {200,200,50,50};

	
	{
		SDL_RenderCopyEx(renderer,
                   	 textTex,
                   	 &srcRect,
                   	 &destRect,
                   	 0,
                   	 NULL,
				   	 SDL_FLIP_NONE);

			
	}

	
	

}

